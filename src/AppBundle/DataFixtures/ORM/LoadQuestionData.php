<?php
/**
 * Data fixture for Question entity.
 *
 * PHP version 5.3
 *
 * @category DataFixtures\ORM
 * @package  AppBundle\DataFixtures\ORM
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
namespace AppBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use AppBundle\Entity\Question;
use AppBundle\Repository\QuestionRepository;
use AppBundle\Repository\TagRepository;
/**
 * Class LoadQuestionData
 *
 * @category DataFixtures\ORM
 * @package  AppBundle\DataFixtures\ORM
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class LoadQuestionData implements FixtureInterface
{

    /**
     * Load function
     *
     * @param ObjectManager $manager Object manager
     * 
     * @return mixed
     */
    public function load(ObjectManager $manager)
    {
        $question = new Question();
        $question->setContent('some content');
        $question->setCreatedAt(new \DateTime());
        $tagRent = $manager->getRepository('AppBundle:Tag')
            ->findOneByName('rent');
        $tagBestseller = $manager->getRepository('AppBundle:Tag')
            ->findOneByName('bestseller');
        $question->addTag($tagRent);
        $question->addTag($tagBestseller);
        //to sledzi obiekty, czy sie nie zmieniaja
        $manager->persist($tagRent);
        $manager->persist($tagBestseller);
        $manager->persist($question);

        $manager->flush();
    }
}