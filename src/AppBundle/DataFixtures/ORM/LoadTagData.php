<?php
/**
 * Data fixture for Tag entity.
 *
 * PHP version 5.3
 *
 * @category DataFixtures\ORM
 * @package  AppBundle\DataFixtures\ORM
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Tag;

/**
 * Class LoadTagData
 *
 * @category DataFixtures\ORM
 * @package  AppBundle\DataFixtures\ORM
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class LoadTagData implements FixtureInterface
{

    /**
 * Load function
     *
     * @param ObjectManager $manager Object manager
     * 
     * @return mixed
     */
    public function load(ObjectManager $manager)
    {
        $tags = array('protected', 'business', 'important', 'school');
        foreach ($tags as $tag) {
            $obj = new Tag();
            $obj->setName($tag);
            $manager->persist($obj);
        }
        $manager->flush();
    }
}