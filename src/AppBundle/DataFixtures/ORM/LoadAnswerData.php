<?php
/**
 * Data fixture for Answer entity.
 *
 * PHP version 5.3
 *
 * @category DataFixtures\ORM
 * @package  AppBundle\DataFixtures\ORM
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
namespace AppBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Answer;
use Doctrine\Common\Persistence\ObjectRepository;
/**
 * Class LoadAnswerData
 * 
 * @category DataFixtures\ORM
 * @package  AppBundle\DataFixtures\ORM
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class LoadAnswerData implements FixtureInterface
{

    /**
 * Load function
     *
     * @param ObjectManager $manager Object manager
     * 
     * @return mixed
     */
    public function load(ObjectManager $manager)
    {
        $answer = new Answer();
        $answer->setContent('some content');
        $answer->setCreatedAt(new \DateTime());
        
        $manager->persist($answer);

        $manager->flush();
    }
}