<?php
/**
 * User entity
 * 
 * PHP version 5.3
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Entity;


use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;


/**
 * User entity class
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 * 
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_users")
 */
class User extends BaseUser
{
    /**
     * User id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Questions array
     *
     * @ORM\OneToMany(
     *      targetEntity="AppBundle\Entity\Question",
     *    mappedBy="user"
     * )
     *
     * @var \Doctrine\Common\Collections\ArrayCollection $questions
     */
    protected $questions;

    /**
     * Questions array
     *
     * @ORM\OneToMany(
     *      targetEntity="AppBundle\Entity\Answer",
     *    mappedBy="user"
     * )
     *
     * @var \Doctrine\Common\Collections\ArrayCollection $answers
     */
    protected $answers;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->roles = array('ROLE_USER');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username Username
     * 
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        $this->usernameCanonical = $username;
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password Password
     * 
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email Mail
     * 
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        $this->emailCanonical = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

 
    /**
     * Get answers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Get questions
     *
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set questions
     *
     * @param Question $question Question
     * 
     * @return mixed
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;
    }
}
