<?php
/**
 * Answer entity
 *
 * PHP version 5.3
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use \DateTime;


/**
 * AppBundle\Entity\Answer
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnswerRepository")
 * @ORM\Table(name="answer")
 * @UniqueEntity(fields="content",
 *      groups={"answer-default"})
 */
class Answer
{
    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned" = true
     *     }
     * )
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer $id
     */
    protected $id;
    /**
     * Content
     *
     * @ORM\Column(
     *     name="content",
     *     type="text",
     *     nullable=false
     * )
     * @Assert\NotBlank(groups={"answer-default"})
     * @Assert\Length(min=3,                       max=128,
     *     groups={"answer-default"})
     *
     * @var string $content
     */
    protected $content;

    /**
     * CreatedAt
     *
     * @ORM\Column(
     *     name="createdAt",
     *     type="datetime",
     *     nullable=false
     * )
     * @Assert\NotBlank(groups={"answer-default"})
     *
     * @var \DateTime $createdAt
     */
    protected $createdAt;

    /**
     * Questions array
     *
     * @ORM\ManyToOne(
     *      targetEntity="Question",
     *      inversedBy="answers"
     * )
     * @ORM\JoinColumn(
     *     name="question_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     *
     * @var \Doctrine\Common\Collections\ArrayCollection $question
     */
    protected $question;


    /**
     * User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",
     *      inversedBy="answers",
     *      cascade={"detach"})
     * @ORM\JoinColumn(name="user_id",
     *     referencedColumnName="id",
     *     nullable=true 
     * )
     */
    protected $user;
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Set id
     *
     * @param string $id id
     *
     * @return Answer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set content
     *
     * @param text $content content
     *
     * @return Answer
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
    /**
     * Get content
     *
     * @return text
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Get user.
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Set user.
     *
     * @param User $user User
     *
     * @return mixed
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt Date
     *
     * @return \DateTime
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set questions
     *
     * @param Question $question Questions
     *
     * @return mixed
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;
    }
    /**
     * Get all records.
     *
     * @access public
     *
     * @return array Answers array
     */
    public function findAll()
    {
        return $this->answers;
    }
    /**
     * Get single record by its id.
     *
     * @param integer $id Single record index
     *
     * @access public
     *
     * @return array Result
     */
    public function find($id)
    {
        if (isset($this->answers[$id]) && count($this->answers)) {
            return $this->answers[$id];
        } else {
            return array();
        }
    }

    /**
     * Delete single record by its id.
     *
     * @param integer $answer Single record index
     *
     * @access public
     *
     * @return array Result
     */
    public function delete($answer)
    {
        return $this->remove($answer);
        //$this->sections->removeElement($sections);
    }
}
