<?php
/**
 * Category entity
 * 
 * PHP version 5.3
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * AppBundle\Entity\Category
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 * @ORM\Table(name="category")
 */
class Category
{
    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned" = true
     *     }
     * )
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer $id
     */
    protected $id;
    /**
     * Name
     *
     * @ORM\Column(
     *     name="name",
     *     type="string",
     *     length=128,
     *     nullable=false
     * )
     * @Assert\NotBlank(groups={"category-default"})
     * @Assert\Length(min=3,max=128,                 groups={"category-default"})
     *
     * @var string $name
     */
    protected $name;


    /**
     * Questions array
     *
     * @ORM\OneToMany(
     *      targetEntity="AppBundle\Entity\Question",
     *    mappedBy="category"
     * )
     */
    protected $questions;
    /**
     * Set id
     *
     * @param string $id id
     * 
     * @return Category
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set name
     *
     * @param string $name name
     * 
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get all records.
     *
     * @access public
     * 
     * @return array Categories array
     */
    public function findAll()
    {
        return $this->categories;
    }
    /**
     * Get single record by its id.
     *
     * @param integer $id Single record index
     *
     * @access public
     *
     * @return array Result
     */
    public function find($id)
    {
        if (isset($this->categories[$id]) && count($this->categories)) {
            return $this->categories[$id];
        } else {
            return array();
        }
    }

    /**
     * Delete single record by its id.
     *
     * @param integer $category Single record index
     *
     * @access public
     *
     * @return array Result
     */
    public function delete($category)
    {
        return $this->remove($category);
        //$this->sections->removeElement($sections);
    }


    /**
     * Add questions.
     *
     * @param \AppBundle\Entity\Question $questions Questions
     * 
     * @return mixed
     */
    public function addQuestion(\AppBundle\Entity\Question $questions)
    {
        $this->questions[] = $questions;
    }
    /**
     * Remove questions
     *
     * @param \AppBundle\Entity\Question $questions Questions
     * 
     * @return mixed
     */
    public function removeAnswer(\AppBundle\Entity\Question $questions)
    {
        $this->questions->removeElement($questions);
    }
    /**
     * Get questions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Remove questions
     *
     * @param \AppBundle\Entity\Question $questions Questions
     * 
     * @return mixed
     */
    public function removeQuestion(\AppBundle\Entity\Question $questions)
    {
        $this->questions->removeElement($questions);
    }
}