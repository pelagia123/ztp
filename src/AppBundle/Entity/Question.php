<?php
/**
 * Question entity
 * 
 * PHP version 5.3
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use \DateTime;

/**
 * AppBundle\Entity\Question
 *
 * Class Question
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 * @ORM\Table(name="questions")
 */
class Question
{

  

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned" = true
     *     }
     * )
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer $id
     */
    protected $id;
    
    /**
     * User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", 
     *     inversedBy="questions", 
     *     cascade={"detach"}
     *     )
     * @ORM\JoinColumn(name="user_id",
     *     referencedColumnName="id", 
     *     nullable=true 
     * )
     */
    protected $user;

    /**
     * Content
     *
     * @ORM\Column(
     *     name="content",
     *     type="text",
     *     nullable=true
     * )
     * @Assert\Length(min=3, groups={"question-default"})
     *
     * @var string $content
     */
    protected $content;
    /**
     * CreatedAt
     *
     * @ORM\Column(
     *     name="createdAt",
     *     type="datetime",
     *     nullable=false
     * )
     * @Assert\NotBlank(groups={"question-default"}))
     *
     * @var \DateTime $createdAt
     */
    protected $createdAt;
    /**
     * Tags array
     *
     * @ORM\ManyToMany(
     *      targetEntity="Tag",
     *      inversedBy="questions"
     * )
     * @ORM\JoinTable(name="questions_tags")
     *
     * @var \Doctrine\Common\Collections\ArrayCollection $tags
     */
    protected $tags;
    /**
     * Categories array
     *
     * @ORM\ManyToOne(
     *      targetEntity="Category",
     *      inversedBy="questions"
     *)
     * @ORM\JoinColumn(name="category_id",
     *      referencedColumnName="id",
     *      onDelete="SET NULL"
     * )
     */
    protected $category;
    /**
     * Answers array
     *
     * @ORM\OneToMany(
     *      targetEntity="Answer",
     *      mappedBy="question"
     * )
     *
     * @var \Doctrine\Common\Collections\ArrayCollection $answers
     */
    protected $answers;
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Set id
     *
     * @param string $id Id
     * 
     * @return Question
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set title
     *
     * @param string $title title
     * 
     * @return Question
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * Set content
     *
     * @param string $content Content
     * 
     * @return Question
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt Date
     * 
     * @return \DateTime
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add tags.
     *
     * @param \AppBundle\Entity\Tag $tags Tag
     * 
     * @return mixed
     */
    public function addTag(\AppBundle\Entity\Tag $tags)
    {
        $this->tags[] = $tags;
    }
    /**
     * Remove tags
     *
     * @param \AppBundle\Entity\Tag $tags tag
     * 
     * @return mixed
     */
    public function removeTag(\AppBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }
    /**
     * Get tags.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }
    /**
     * Get category.
     *
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }
    /**
     * Set category.
     *
     * @param Category $category category
     * 
     * @return mixed
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }
    /**
     * Get user.
     *
     * @return \mysqli
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Set user.
     *
     * @param User $user User
     * 
     * @return mixed
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }
    /**
     * Remove category
     *
     * @param \AppBundle\Entity\Category $category Category
     * 
     * @return mixed
     */
    public function removeCategory(\AppBundle\Entity\Category $category)
    {
        $this->category->removeElement(category);
    }
    /**
     * Add answers.
     *
     * @param \AppBundle\Entity\Answer $answers Answer
     * 
     * @return mixed
     */
    public function addAnswer(\AppBundle\Entity\Answer $answers)
    {
        $this->answers[] = $answers;
    }
    /**
     * Remove answers
     *
     * @param \AppBundle\Entity\Answer $answers Answer
     * 
     * @return mixed
     */
    public function removeAnswer(\AppBundle\Entity\Answer $answers)
    {
        $this->answers->removeElement($answers);
    }
    /**
     * Get answers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
    /**
     * Get all records.
     *
     * @access public
     * 
     * @return array Questions array
     */
    public function findAll()
    {
        return $this->questions;
    }
    /**
     * Get single record by its id.
     *
     * @param integer $id Single record index
     *
     * @access public
     *
     * @return array Result
     */
    public function find($id)
    {
        if (isset($this->questions[$id]) && count($this->questions)) {
            return $this->questions[$id];
        } else {
            return array();
        }
    }

    /**
     * Delete single record by its id.
     *
     * @param integer $question Single record index
     *
     * @access public
     *
     * @return array Result
     */
    public function delete($question)
    {
        return $this->remove($question);
        //$this->sections->removeElement($sections);
    }

}