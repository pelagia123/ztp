<?php
/**
 * Tag entity.
 *
 * PHP version 5.3
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag.
 *
 * @category Entity
 * @package  Model
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 *
 * @ORM\Table(name="tags")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TagRepository")
 */
class Tag
{
    /**
     * Tag id
     *
     * @ORM\Id
     * @ORM\Column(
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned" = true
     *     }
     * )
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * Tag name
     *
     * @ORM\Column(
     *     name="name",
     *     type="string",
     *     length=128,
     *     nullable=false
     * )
     */
    protected $name;
    /**
     * Questions array
     *
     * @ORM\ManyToMany(targetEntity="Question", mappedBy="tags")
     *
     * @var \Doctrine\Common\Collections\ArrayCollection $questions
     */
    protected $questions;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Set id
     *
     * @param string $id id
     * 
     * @return Tag
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set name
     *
     * @param string $name name
     * 
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add questions.
     *
     * @param \AppBundle\Entity\Question $questions Question
     * 
     * @return mixed
     */
    public function addQuestion(\AppBundle\Entity\Question $questions)
    {
        $this->questions[] = $questions;
    }
    /**
     * Remove questions
     *
     * @param \AppBundle\Entity\Question $questions Questions
     * 
     * @return mixed 
     */
    public function removeQuestion(\AppBundle\Entity\Question $questions)
    {
        $this->questions->removeElement($questions);
    }
    /**
     * Get questions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Get all records.
     *
     * @access public
     * 
     * @return array Tags array
     */
    public function findAll()
    {
        return $this->tags;
    }
    /**
     * Get single record by its id.
     *
     * @param integer $id Single record index
     *
     * @access public
     *
     * @return array Result
     */
    public function find($id)
    {
        if (isset($this->tags[$id]) && count($this->tags)) {
            return $this->tags[$id];
        } else {
            return array();
        }
    }

    /**
     * Delete single record by its id.
     *
     * @param integer $tag Single record index
     *
     * @access public
     *
     * @return array Result
     */
    public function delete($tag)
    {
        return $this->remove($tag);

    }

}