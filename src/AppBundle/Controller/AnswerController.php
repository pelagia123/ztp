<?php
/**
 * Created by PhpStorm.
 * User: pelcia
 *
 * PHP version 5.3
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
namespace AppBundle\Controller;


use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Form\AnswerType;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;



/**
 * Class AnswerController
 *
 * @Route (service="app.answers_controller")
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class AnswerController
{

    /**
     * Translator object.
     *
     * @var Translator $translator
     */
    protected $translator;

    /**
     * Template engine.
     *
     * @var EngineInterface $templating
     */
    protected $templating;

    /**
     * Session object.
     *
     * @var Session $session
     */
    protected $session;

    /**
     * Routing object.
     *
     * @var RouterInterface $router
     */
    protected $router;

    /**
     * Security context
     *
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * Model object.
     *
     * @var ObjectRepository $model
     */
    protected $model;
    /**
     * Answer model object.
     *
     * @var ObjectRepository $answersModel
     */
    protected $answersModel;
    /**
     * Questions model object.
     *
     * @var ObjectRepository $questionsModel
     */
    protected $questionsModel;

    /**
     * Form factory.
     *
     * @var FormFactory $formFactory
     */
    protected $formFactory;

    /**
     * AnswerController constructor.
     *
     * @param Translator       $translator      Translator
     * @param EngineInterface  $templating      Templating engine
     * @param Session          $session         Session
     * @param RouterInterface  $router          Router interface
     * @param SecurityContext  $securityContext SecurityContext
     * @param ObjectRepository $questionsModel  Model object
     * @param ObjectRepository $answersModel    Model object
     * @param FormFactory      $formFactory     Form factory
     */
    public function __construct(
        Translator $translator,
        EngineInterface $templating,
        Session $session,
        RouterInterface $router,
        SecurityContext $securityContext,
        ObjectRepository $questionsModel,
        ObjectRepository $answersModel,
        FormFactory $formFactory
    ) {
    
        $this->translator = $translator;
        $this->templating = $templating;
        $this->session = $session;
        $this->router = $router;
        $this->securityContext = $securityContext;
        $this->questionsModel = $questionsModel;
        $this->answersModel = $answersModel;
        $this->formFactory = $formFactory;
    }




    /**
     * Get all answers.
     *
     * @Route("/answers",  name="answer_index")
     * @Route("/answers/")
     *
     * @Method("GET")
     * @Template()
     *
     * @return mixed
     */
    public function indexAction()
    {
        $answers = $this->answersModel->findAll();
        if (!$answers) {
            throw new NotFoundHttpException('Answers not found!');
        }
        return $this->templating->renderResponse(
            'AppBundle:Answer:index.html.twig',
            array('answers' => $answers)
        );
    }

    /**
     * View action.
     *
     * @param Answer $answer Answer entity
     * 
     * @Route("/answers/view/{id}",  name="answers_view")
     * @Route("/answers/view/{id}/")
     * @ParamConverter("answer",     class="AppBundle:Answer")
     *
     * @throws NotFoundHttpException
     * 
     * @return Response A Response instance
     */
    public function viewAction(Answer $answer = null)
    {
        if (!$answer) {
            throw new NotFoundHttpException(
                $this->translator->trans('answers.messages.answer_not_found')
            );
        }
        return $this->templating->renderResponse(
            'AppBundle:Answer:show.html.twig',
            array('answer' => $answer)
        );
    }

    /**
     * Add action
     *
     * @param Request  $request  Request
     * @param question $question question entity
     *
     * @Route("/questions/{id}/answers/add",  name="answer_new")
     * @Route("/questions/{id}/answers/add/")
     * @ParamConverter("question",            class="AppBundle:Question")
     *
     * @return Response A Response instance
     */
    public function addAction(Request $request, Question $question =null)
    {
        $id = (integer)$request->get('id', null);
        $answer = new Answer();
        $answer->setQuestion($question);
        
        $answerForm = $this->formFactory->create(
            new AnswerType(),
            $answer,
            array(
                'validation_groups'=>'answer-default',
                

            )
        );

        $answerForm->handleRequest($request);
        
        if ($answerForm->isValid()) {
            $user = $this->securityContext->getToken()->getUser();
            $answer = $answerForm->getData();
            $this->answersModel->save($answer, $user);
            $this->session->getFlashBag()->set(
                'success',
                $this->translator->trans('answers.messages.success.add')
            );
            return new RedirectResponse(
                $this->router->generate('questions_view', array('id' => $id))
            );
        }
        
        
        

        return $this->templating->renderResponse(
            'AppBundle:Answer:new.html.twig', array(
            'form' => $answerForm->createView()
            )
        );
    }

    /**
     * Delete action.
     *
     * @param Request $request Request
     * @param Answer  $answer  Answer entity
     *
     * @Route("/answers/delete/{id}",  name="answers_delete")
     * @Route("/answers/delete/{id}/")
     * @ParamConverter("answer",       class="AppBundle:Answer")
     *
     * @return Response A Response instance
     */
    public function deleteAction(Request $request, Answer $answer = null)
    {
        if (!$answer) {
            $this->session->getFlashBag()->set(
                'warning',
                $this->translator->trans('answers.messages.answer_not_found')
            );
            return new RedirectResponse(
                $this->router->generate('answers')
            );
        }

        $answerForm = $this->formFactory->create(
            new AnswerType(),
            $answer,
            array(
                'validation_groups' => 'answer-delete'
            )
        );

        $answerForm->handleRequest($request);

        if ($answerForm->isValid()) {
            $user = $this->securityContext->getToken()->getUser();
            $answer = $answerForm->getData();
            $this->answersModel->delete($answer, $user);
            $this->session->getFlashBag()->set(
                'success',
                $this->translator->trans('answers.messages.success.delete')
            );
            return new RedirectResponse(
                $this->router->generate('answer_index')
            );
        }

        return $this->templating->renderResponse(
            'AppBundle:Answer:delete.html.twig',
            array('form' => $answerForm->createView())
        );

    }


    /**
     * Edit action.
     *
     * @param Request $request Request
     * @param Answer  $answer  Answer entity
     *
     * @Route("/answers/edit/{id}",  name="answers_edit")
     * @Route("/answers/edit/{id}/")
     * @ParamConverter("answer",     class="AppBundle:Answer")
     *
     * @return Response A Response instance
     */
    public function editAction(Request $request, Answer $answer = null)
    {
        if (!$answer) {
            $this->session->getFlashBag()->set(
                'warning',
                $this->translator->trans('answers.messages.answer_not_found')
            );
            return new RedirectResponse(
                $this->router->generate('answers-add')
            );
        }

        $answerForm = $this->formFactory->create(
            new AnswerType(),
            $answer,
            array(
                'validation_groups' => 'answer-default'
            )
        );

        $answerForm->handleRequest($request);

        if ($answerForm->isValid()) {
            $user = $this->securityContext->getToken()->getUser();
            $answer = $answerForm->getData();
            $this->answersModel->save($answer, $user);
            $this->session->getFlashBag()->set(
                'success',
                $this->translator->trans('answers.messages.success.edit')
            );
            return new RedirectResponse(
                $this->router->generate('answer_index')
            );
        }

        return $this->templating->renderResponse(
            'AppBundle:Answer:edit.html.twig',
            array('form' => $answerForm->createView())
        );

    }


}
