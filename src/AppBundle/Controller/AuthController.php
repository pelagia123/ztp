<?php
/**
 * Created by PhpStorm.
 * User: pelcia
 * Date: 14.05.16
 * Time: 19:35
 * 
 *  PHP version 5.3
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\FOSUserBundle as BaseUser;
use FOS\UserBundle\Controller\SecurityController;

/**
 * Class AuthController
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class AuthController extends SecurityController
{
    /**
     * Login
     * 
     * @Route("/login", name="auth_login")
     * @Template()
     * @return          mixed
     */
    public function loginAction()
    {
        $request = $this->container->get('request');
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $session = $request->getSession();
        /* @var $session \Symfony\Component\HttpFoundation\Session\Session */

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } elseif (null !== $session && $session->has(
            SecurityContext::AUTHENTICATION_ERROR
        )) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {
            // TODO: this is a potential security risk
            // TODO: (see http://trac.symfony-project.org/ticket/9523)
            $error = $error->getMessage();
        }
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(
            SecurityContext::LAST_USERNAME
        );
        
        $csrfToken = $this->container->get('form.csrf_provider')->generateCsrfToken(
            'authenticate'
        );

        return $this->renderLogin(
            array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'csrf_token' => $csrfToken,
            )
        );
    }

    /**
     * Login check
     * 
     * @Route("/login_check", name="auth_login_check")
     * @Template()
     * @return                mixed
     */
    public function loginCheckAction()
    {
        // The security layer will intercept this request
    }

    /**
     * Logout
     *
     * @Route("/logout", name="auth_logout")
     * @return           mixed
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }

    /**
     * Access denied
     *
     * @Route("/access-denied")
     * @Template()
     * @return                  mixed
     */
    public function accessDeniedAction()
    {
        exit('Brak dostępu!');
    }


    /**
     * Admin
     *
     * @Route("/admin/", name="admin_page")
     * @Template()
     * @return           mixed
     */
    public function adminAction()
    {

    }



}

