<?php
/**
 * Created by PhpStorm.
 * User: pelcia
 * PHP version 5.3
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
namespace AppBundle\Controller;


use AppBundle\Entity\Question;
use AppBundle\Form\QuestionType;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;


/**
 * Class QuestionController
 * 
 * @Route (service="app.questions_controller")
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class QuestionController
{

    /**
     * Translator object.
     *
     * @var Translator $translator
     */
    protected $translator;
    /**
     * Template engine.
     *
     * @var EngineInterface $templating
     */
    protected $templating;
    /**
     * Session object.
     *
     * @var Session $session
     */
    protected $session;
    /**
     * Routing object.
     *
     * @var RouterInterface $router
     */
    protected $router;

    /**
     * Security context
     *
     * @var SecurityContext
     */
    protected $securityContext;
    /**
     * Questions model object.
     *
     * @var ObjectRepository $questionsModel
     */
    protected $questionsModel;

    /**
     * Tags model object.
     *
     * @var ObjectRepository $tagsModel
     */
    protected $tagsModel;

    /**
     * Categories model object.
     *
     * @var ObjectRepository $categoriesModel
     */
    protected $categoriesModel;
    /**
     * Answers model object.
     *
     * @var ObjectRepository $answersModel
     */
    protected $answersModel;

    /**
     * Form factory.
     *
     * @var FormFactory $formFactory
     */
    protected $formFactory;
    /**
     * QuestionsController constructor.
     *
     * @param Translator       $translator      Translator
     * @param EngineInterface  $templating      Templating engine
     * @param Session          $session         Session
     * @param RouterInterface  $router          Router interface
     * @param SecurityContext  $securityContext SecurityContext
     * @param ObjectRepository $questionsModel  Model object
     * @param ObjectRepository $tagsModel       Model object
     * @param ObjectRepository $categoriesModel Model object
     * @param ObjectRepository $answersModel    Model object
     * @param FormFactory      $formFactory     Form factory
     */
    public function __construct(
        Translator $translator,
        EngineInterface $templating,
        Session $session,
        RouterInterface $router,
        SecurityContext $securityContext,
        ObjectRepository $questionsModel,
        ObjectRepository $tagsModel,
        ObjectRepository $categoriesModel,
        ObjectRepository $answersModel,
        FormFactory $formFactory
    ) {
        $this->translator = $translator;
        $this->templating = $templating;
        $this->session = $session;
        $this->router = $router;
        $this->securityContext = $securityContext;
        $this->questionsModel = $questionsModel;
        $this->tagsModel = $tagsModel;
        $this->categoriesModel = $categoriesModel;
        $this->answersModel = $answersModel;
        $this->formFactory = $formFactory;
    }

 
    /**
     * Get all questions.
     *
     * @Route("/questions",  name="question_index")
     * @Route("/questions/")
     * @Route("/")
     *
     * @Method("GET")
     * @Template()
     * @return        mixed
     */
    public function indexAction()
    {
        $questions = $this->questionsModel->findAll();
        if (!$questions) {
            throw new NotFoundHttpException('Questions not found!');
        }
        return $this->templating->renderResponse(
            'AppBundle:Question:index.html.twig',
            array('questions' => $questions)
        );
    }

    /**
     * View action.
     *
     * @param Question $question Question entity
     * 
     * @Route("/questions/view/{id}",  name="questions_view")
     * @Route("/questions/view/{id}/")
     * @ParamConverter("question",     class="AppBundle:Question")
     *
     * @throws NotFoundHttpException
     * 
     * @return Response A Response instance
     */
    public function viewAction(Question $question = null)
    {
        $tags = $question->getTags();
        $category = $question->getCategory();
        $answers = $question->getAnswers();
        
        if (!$question) {
            throw new NotFoundHttpException(
                $this->translator->trans('questions.messages.question_not_found')
            );
        }
        return $this->templating->renderResponse(
            'AppBundle:Question:show.html.twig',
            array('question' => $question,
                'tags'=>$tags,
                'category'=>$category, 
                'answer'=>$answers)
        );
    }

    /**
     * Add action.
     *
     * @param Request $request Request
     *
     * @Route("/questions/add",  name="questions_new")
     * @Route("/questions/add/")
     *
     * @return Response A Response instance
     */
    public function addAction(Request $request)
    {
        $questionForm = $this->formFactory->create(
            new QuestionType(),
            null,
            array(
                'validation_groups' => 'question-default',
                'tag_model' => $this->tagsModel,
                'category_model' => $this->categoriesModel
            )
        );
        $questionForm->handleRequest($request);
        if ($questionForm->isValid()) {
            $user = $this->securityContext->getToken()->getUser();
            $question = $questionForm->getData();
            $this->questionsModel->save($question, $user);
            $this->session->getFlashBag()->set(
                'success',
                $this->translator->trans('questions.messages.success.add')
            );
            return new RedirectResponse(
                $this->router->generate('question_index')
            );
        }
        return $this->templating->renderResponse(
            'AppBundle:Question:new.html.twig',
            array('form' => $questionForm->createView())
        );
    }

    /**
     * Delete action.
     *
     * @param Request  $request  Request
     * @param Question $question Question entity
     *
     * @Route("/questions/delete/{id}",  name="questions_delete")
     * @Route("/questions/delete/{id}/")
     * @ParamConverter("question",       class="AppBundle:Question")
     *
     * @return Response A Response instance
     */
    public function deleteAction(Request $request, Question $question = null)
    {
        if (!$question) {
            $this->session->getFlashBag()->set(
                'warning',
                $this->translator->trans('questions.messages.question_not_found')
            );
            return new RedirectResponse(
                $this->router->generate('questions_new')
            );
        }

        $questionForm = $this->formFactory->create(
            new QuestionType(),
            $question,
            array(
                'validation_groups' => 'question-delete',
                'tag_model' => $this->tagsModel,
                'category_model' => $this->categoriesModel
            )
        );

        $questionForm->handleRequest($request);

        if ($questionForm->isValid()) {
            $user = $this->securityContext->getToken()->getUser();
            $question = $questionForm->getData();
            $this->questionsModel->delete($question, $user);
            $this->session->getFlashBag()->set(
                'success',
                $this->translator->trans('questions.messages.success.delete')
            );
            return new RedirectResponse(
                $this->router->generate('question_index')
            );
        }

        return $this->templating->renderResponse(
            'AppBundle:Question:delete.html.twig',
            array('form' => $questionForm->createView())
        );

    }


    /**
     * Edit action.
     *
     * @param Request  $request  Request
     * @param Question $question Question entity
     *
     * @Route("/questions/edit/{id}",  name="questions_edit")
     * @Route("/questions/edit/{id}/")
     * @ParamConverter("question",     class="AppBundle:Question")
     *
     * @return Response A Response instance
     */
    public function editAction(Request $request, Question $question = null)
    {
        if (!$question) {
            $this->session->getFlashBag()->set(
                'warning',
                $this->translator->trans('questions.messages.question_not_found')
            );
            return new RedirectResponse(
                $this->router->generate('questions_new')
            );
        }

        $questionForm = $this->formFactory->create(
            new QuestionType(),
            $question,
            array(
                'validation_groups' => 'question-default',
                'tag_model' => $this->tagsModel,
                'category_model' => $this->categoriesModel
            )
        );

        $questionForm->handleRequest($request);

        if ($questionForm->isValid()) {
            $user = $this->securityContext->getToken()->getUser();
            $question = $questionForm->getData();
            $this->questionsModel->save($question, $user);
            $this->session->getFlashBag()->set(
                'success',
                $this->translator->trans('questions.messages.success.edit')
            );
            return new RedirectResponse(
                $this->router->generate('question_index')
            );
        }

        return $this->templating->renderResponse(
            'AppBundle:Question:edit.html.twig',
            array('form' => $questionForm->createView())
        );

    }

    
}
