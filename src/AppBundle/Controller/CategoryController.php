<?php
/**
 * Category controller class.
 *
 * PHP version 5.3
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use Doctrine\Common\Persistence\ObjectRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class CategoryController.
 *
 * @Route(service="app.categories_controller")
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class CategoryController
{
    /**
     * Translator object.
     *
     * @var Translator $translator
     */
    protected $translator;

    /**
     * Template engine.
     *
     * @var EngineInterface $templating
     */
    protected $templating;

    /**
     * Session object.
     *
     * @var Session $session
     */
    protected $session;

    /**
     * Routing object.
     *
     * @var RouterInterface $router
     */
    protected $router;

    /**
     * Model object.
     *
     * @var ObjectRepository $model
     */
    protected $model;

    /**
     * Form factory.
     *
     * @var FormFactory $formFactory
     */
    protected $formFactory;

    /**
        * CategoryController constructor.
        *
        * @param Translator       $translator  Translator
        * @param EngineInterface  $templating  Templating engine
        * @param Session          $session     Session
        * @param RouterInterface  $router      Router interface
        * @param ObjectRepository $model       Model object
        * @param FormFactory      $formFactory Form factory
        */
    public function __construct(
        Translator $translator,
        EngineInterface $templating,
        Session $session,
        RouterInterface $router,
        ObjectRepository $model,
        FormFactory $formFactory
    ) {
       
        $this->translator = $translator;
        $this->templating = $templating;
        $this->session = $session;
        $this->router = $router;
        $this->model = $model;
        $this->formFactory = $formFactory;
    }

    /**
     * Index action.
     *
     * @Route("/categories",  name="categories_index")
     * @Route("/categories/")
     *
     * @throws NotFoundHttpException
     * @return Response A Response instance
     */
    public function indexAction()
    {
        $categories = $this->model->findAll();
        if (!$categories) {
            throw new NotFoundHttpException(
                $this->translator->trans('categories.messages.categories_not_found')
            );
        }
        return $this->templating->renderResponse(
            'AppBundle:Categories:index.html.twig',
            array('categories' => $categories)
        );
    }

    /**
     * View action.
     *
     * @param Category $category Category entity
     * 
     * @Route("/categories/view/{id}",  name="categories-view")
     * @Route("/categories/view/{id}/")
     * @ParamConverter("categories",    class="AppBundle:Category")
     *
     * @throws NotFoundHttpException
     * 
     * @return Response A Response instance
     */
    public function viewAction(Category $category = null)
    {
        if (!$category) {
            throw new NotFoundHttpException(
                $this->translator->trans('categories.messages.categories_not_found')
            );
        }
        return $this->templating->renderResponse(
            'AppBundle:Categories:view.html.twig',
            array('category' => $category)
        );
    }

    /**
     * Add action.
     *
     * @param Request $request Request
     * 
     * @Route("/categories/add",  name="categories-add")
     * @Route("/categories/add/")
     *
     * @return Response A Response instance
     */
    public function addAction(Request $request)
    {
        $categoryForm = $this->formFactory->create(
            new CategoryType(),
            null,
            array(
                'validation_groups' => 'category-default'
            )
        );

        $categoryForm->handleRequest($request);

        if ($categoryForm->isValid()) {
            $category = $categoryForm->getData();
            $this->model->save($category);
            $this->session->getFlashBag()->set(
                'success',
                $this->translator->trans('categories.messages.success.add')
            );
            return new RedirectResponse(
                $this->router->generate('categories_index')
            );
        }

        return $this->templating->renderResponse(
            'AppBundle:Categories:add.html.twig',
            array('form' => $categoryForm->createView())
        );
    }

    /**
     * Edit action.
     *
     * @param Request  $request  Request
     * @param Category $category Category entity
     *
     * @Route("/categories/edit/{id}",  name="categories-edit")
     * @Route("/categories/edit/{id}/")
     * @ParamConverter("category",      class="AppBundle:Category")
     *
     * @return Response A Response instance
     */
    public function editAction(Request $request, Category $category = null)
    {
        if (!$category) {
            $this->session->getFlashBag()->set(
                'warning',
                $this->translator->trans('categories.messages.category_not_found')
            );
            return new RedirectResponse(
                $this->router->generate('categories-add')
            );
        }

        $categoryForm = $this->formFactory->create(
            new CategoryType(),
            $category,
            array(
                'validation_groups' => 'category-default'
            )
        );

        $categoryForm->handleRequest($request);

        if ($categoryForm->isValid()) {
            $category = $categoryForm->getData();
            $this->model->save($category);
            $this->session->getFlashBag()->set(
                'success',
                $this->translator->trans('categories.messages.success.edit')
            );
            return new RedirectResponse(
                $this->router->generate('categories_index')
            );
        }

        return $this->templating->renderResponse(
            'AppBundle:Categories:edit.html.twig',
            array('form' => $categoryForm->createView())
        );

    }

    /**
     * Delete action.
     *
     * @param Request  $request  Request
     * @param Category $category Category entity
     *
     * @Route("/categories/delete/{id}",  name="categories-delete")
     * @Route("/categories/delete/{id}/")
     * @ParamConverter("category",        class="AppBundle:Category")
     *
     * @return Response A Response instance
     */
    public function deleteAction(Request $request, Category $category = null)
    {
        if (!$category) {
            $this->session->getFlashBag()->set(
                'warning',
                $this->translator->trans('categories.messages.category_not_found')
            );
            return new RedirectResponse(
                $this->router->generate('categories_index')
            );
        }

        $categoryForm = $this->formFactory->create(
            new CategoryType(),
            $category,
            array(
                'validation_groups' => 'category-delete'
            )
        );

        $categoryForm->handleRequest($request);

        if ($categoryForm->isValid()) {
            $category = $categoryForm->getData();
            $this->model->delete($category);
            $this->session->getFlashBag()->set(
                'success',
                $this->translator->trans('categories.messages.success.delete')
            );
            return new RedirectResponse(
                $this->router->generate('categories_index')
            );
        }

        return $this->templating->renderResponse(
            'AppBundle:Categories:delete.html.twig',
            array('form' => $categoryForm->createView())
        );

    }

}