<?php
/**
 * Default controller class.
 * PHP version 5.3
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class DefaultController.
 *
 * @category Controller
 * @package  AppBundle\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class DefaultController extends Controller
{
    /**
     * Index action.
     *
     * @param string $name Name
     * 
     * @Route("/hello/{name}")
     *
     * @return Response A Response instance
     */
    public function indexAction($name)
    {
        return $this->render(
            'AppBundle:Default:index.html.twig',
            array('name' => $name)
        );
    }

}
