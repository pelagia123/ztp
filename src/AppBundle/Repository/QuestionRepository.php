<?php
/**
 * Question repository
 * PHP version 5.3
 *
 * @category Repository
 * @package  AppBundle\Entity
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Question;

/**
 * QuestionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 *
 * Class Question
 *
 * @category Repository
 * @package  AppBundle\Entity
 * @author   Katarzyna Puczko <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class QuestionRepository extends EntityRepository
{
    /**
     * Save question object.
     *
     * @param Question $question Question object
     * @param User     $user     User object
     * 
     * @return mixed
     */
    public function save(\AppBundle\Entity\Question $question, $user)
    {
        $question->setUser($user);
        $this->_em->persist($question);
        $this->_em->flush();
    }
    /**
     * Delete question object.
     *
     * @param Question $question Question object
     * 
     * @return mixed
     */
    public function delete(\AppBundle\Entity\Question $question)
    {
        
        $this->_em->remove($question);
        $this->_em->flush();
    }
    
    
    

}
