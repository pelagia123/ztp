<?php
/**
 * Tag repository.
 *
 * PHP version 5.3
 *
 * @category Repository
 * @package  AppBundle\Entity
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Tag;

/**
 * Class TagRepository
 *
 * @category Repository
 * @package  AppBundle\Entity
 * @author   Katarzyna Puczko <katarzyna.puczkko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class TagRepository extends EntityRepository
{
    /**
     * Save tag object.
     *
     * @param Tag $tag Tag object
     * 
     * @return mixed
     */
    public function save(\AppBundle\Entity\Tag $tag)
    {
        $this->_em->persist($tag);
        $this->_em->flush();
    }
    /**
     * Delete tag object.
     *
     * @param Tag $tag Tag object
     * 
     * @return mixed
     */
    public function delete(\AppBundle\Entity\Tag $tag)
    {
        $this->_em->remove($tag);
        $this->_em->flush();
    }
}