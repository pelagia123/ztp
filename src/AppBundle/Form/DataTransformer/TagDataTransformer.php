<?php
/**
 * Tag data transformer.
 *
 * PHP version 5.3
 *
 * @category Form\DataTransformer
 * @package  AppBundle\Form\DataTransformer
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Form\DataTransformer;

use AppBundle\Entity\Tag;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class TagDataTransformer.
 *
 * @category Form\DataTransformer
 * @package  AppBundle\Form\DataTransformer
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class TagDataTransformer implements DataTransformerInterface
{
    /**
     * Model object.
     *
     * @var ObjectRepository $model
     */
    protected $model = null;

    /**
     * TagDataTransformer constructor.
     *
     * @param ObjectRepository $model Model repository
     */
    public function __construct(ObjectRepository $model)
    {
        $this->model = $model;
    }

    /**
     * Transform.
     *
     * @param array $tags Array of tag objects
     *
     * @return string Result
     */
    public function transform($tags)
    {
        if (!$tags) {
            return '';
        }

        $result = array();

        foreach ($tags as $tag) {
            $result[] = $tag->getName();
        }

        return join(', ', $result);

    }

    /**
     * Reversed transform.
     *
     * @param string $tags Tag names
     *
     * @return array Result
     */
    public function reverseTransform($tags)
    {
        if (!$tags) {
            return array();
        }

        $result = array();
        $tagsNames = explode(',', $tags);

        foreach ($tagsNames as $name) {
            $name = trim($name);

            $tag = $this->model->findOneByName($name);

            if (!$tag) {
                $tag = new Tag();
                $tag->setName($name);
                $this->model->save($tag);
            }

            $result[] = $tag;
        }

        return $result;
    }

}