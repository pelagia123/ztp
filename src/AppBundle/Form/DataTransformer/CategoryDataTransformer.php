<?php
/**
 * Category data transformer.
 *
 * PHP version 5.3
 *
 * @category Form\DataTransformer
 * @package  AppBundle\Form\DataTransformer
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */

namespace AppBundle\Form\DataTransformer;

use AppBundle\Entity\Category;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class CategoryDataTransformer.
 *
 * @category Form\DataTransformer
 * @package  AppBundle\Form\DataTransformer
 * @author   Katarzyna Puczko <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class CategoryDataTransformer implements DataTransformerInterface
{
    /**
     * Model object.
     *
     * @var ObjectRepository $model
     */
    protected $model = null;

    /**
     * CategoryDataTransformer constructor.
     *
     * @param ObjectRepository $model Model repository
     */
    public function __construct(ObjectRepository $model)
    {
        $this->model = $model;
    }

    /**
     * Transform.
     *
     * @param array $categories Array of category objects
     *
     * @return string Result
     */
    public function transform($categories)
    {
        if (!$categories) {
            return '';
        }

        $result = array();

        foreach ($categories as $category) {
            $result[] = $category->getName();
        }

        return join(', ', $result);

    }

    /**
     * Reversed transform.
     *
     * @param string $categories Category names
     *
     * @return array Result
     */
    public function reverseTransform($categories)
    {
        if (!$categories) {
            return array();
        }

        $result = array();
        $categoriesNames = explode(',', $categories);

        foreach ($categoriesNames as $name) {
            $name = trim($name);

            $category = $this->model->findOneByName($name);

            if (!$category) {
                $category = new Category();
                $category->setName($name);
                $this->model->save($category);
            }

            $result[] = $category;
        }

        return $result;
    }

}