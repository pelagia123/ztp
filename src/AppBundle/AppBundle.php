<?php
/**
 * PHP version 5.3
 *
 * @category AppBundle
 * @package  AppBundle
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AppBundle
 * 
 * @category AppBundle
 * @package  AppBundle
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class AppBundle extends Bundle
{
}
