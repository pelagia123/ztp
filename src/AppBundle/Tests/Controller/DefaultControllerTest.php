<?php
/**
 * Default controller test
 * 
 *  PHP version 5.3
 *
 * @category Test
 * @package  AppBundle\Test\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest
 *
 * @category Test
 * @package  AppBundle\Test\Controller
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     * Defaault test
     * 
     * @return mixed
     */
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/hello/Fabien');
        $this->assertTrue(
            $crawler->filter(
                'html:contains("Hello Fabien")'
            )->count() > 0
        );
    }
}
