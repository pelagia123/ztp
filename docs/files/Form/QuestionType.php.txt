<?php
/**
 * Question type
 * 
 * PHP version 5.3
 *
 * @category Form
 * @package  AppBundle\Form
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
namespace AppBundle\Form;

use AppBundle\Entity\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\DataTransformer\CategoryDataTransformer;
use AppBundle\Form\DataTransformer\TagDataTransformer;

/**
 * Class QuestionType
 *
 * @category Form
 * @package  AppBundle\Form
 * @author   Katarzyna Puczko  <katarzyna.puczko92@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://wierzba.wzks.uj.edu.pl/~12_puczko/aplikacja/app_dev.php/
 */
class QuestionType extends AbstractType
{

    /**
     * Form builder.
     *
     * @param FormBuilderInterface $builder Form builder
     * @param array                $options Form options
     * 
     * @return mixed
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tagDataTransformer = new TagDataTransformer($options['tag_model']);
        $categoryDataTransformer = new CategoryDataTransformer(
            $options['category_model']
        );
        
        $builder->add(
            'id',
            'hidden'
        );
        if (isset($options['validation_groups'])
            && count($options['validation_groups'])
            && !in_array('question-delete', $options['validation_groups'])
        ) {
           
            $builder->add(
                'content',
                'textarea',
                array(
                    'label'      => 'Question content',
                    'required'   => false,
                    
                )
            );
            // $builder->add(
            // $builder
            // ->create('tags', 'text')
            // ->addModelTransformer($tagDataTransformer),
            // array(
            // 'required'   => false,
            // )
            // );
            $builder->add(
                'tags',
                'entity',
                array(
                    'class' => 'AppBundle:Tag',
                    'property' => 'name',
                    'multiple' => true,
                    'expanded' => true,
                    'empty_data'  => null
                )
            );
            $builder->add(
                'category',
                'entity',
                array(
                    // query choices from this entity
                    'class' => 'AppBundle:Category',
                    // use the User.username property as the visible option string
                    'property' => 'name',
                    // used to render a select box, check boxes or radios
                    'multiple' => false,
                    'expanded' => true,
                    'empty_data'  => null
                )
            );
            // $builder->add(
            // $builder
            // ->create('categories', 'text')
            // ->addModelTransformer($categoryDataTransformer)
            // );
        }
        $builder->add(
            'save',
            'submit',
            array(
                'label' => 'Save'
            )
        );
    }
    /**
     * Sets default options for form.
     *  
     * @param OptionsResolverInterface $resolver resolver
     * 
     * @return mixed
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Question',
                'validation_groups' => 'question-default',
            )
        );
        
        $resolver->setRequired(array('tag_model', 'category_model'));
        $resolver->setAllowedTypes(
            array(
                'tag_model' => 'Doctrine\Common\Persistence\ObjectRepository',
                'category_model' => 'Doctrine\Common\Persistence\ObjectRepository'
            )
        );

    }

    //    /**
    //     * @param Request $request
    //     */
    //    public function newAction(Request $request)
    //    {
    //        $question = new Question();
    //        $form = $this->createForm(QuestionType::class, $question);
    //
    //        
    //    }

    /**
     * Getter for form name.
     *
     * @return string Form name
     */
    public function getName()
    {
        return 'question_form';
    }
}
